

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"
        import ="java.sql.Connection"        
        import ="java.sql.DriverManager"        
        import ="java.sql.ResultSet"        
        import ="java.sql.Statement"        
        import ="java.sql.SQLException"        
        %>



<%
    Connection conex = null;
    PreparedStatement ps;
    Statement sql = null;
    String URL = "jdbc:mysql://127.0.0.1/Catalogo?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";
    String usuario = "root";
    String contraseña = "root";
    Date fecha = new Date();
    DateFormat fechaHora = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    try {
        Class.forName("com.mysql.cj.jdbc.Driver");
        conex = (Connection) DriverManager.getConnection(URL, usuario, contraseña);
        out.print("Conexión establecida");

        ps = conex.prepareStatement("UPDATE Videojuegos SET  Juego=?, Descripcion=?, Calificacion=?, Fecha=? where idVideojuegos=?");
        ps.setString(1, request.getParameter("juego"));
        ps.setString(2, request.getParameter("descripcion"));
        ps.setString(3, request.getParameter("calificacion"));
        ps.setString(4, fechaHora.format(fecha));
        ps.setString(5, request.getParameter("idVideojuegos"));

        ps.executeUpdate();
        response.sendRedirect("ListadeJuegos.jsp");

    } catch (Exception e) {
        out.print("Error en la conexión");
        e.printStackTrace();
    }

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Bienvenidos!</h1>
    </body>
</html>
